<?php

use yii\db\Migration;
use yii\base\InvalidConfigException;
use app\common\rbac\DefaultRoleRule;

class m160405_124828_add_access_rules extends Migration
{
    /**
     * @var \yii\rbac\ManagerInterface
     */
    protected $auth;
    
    public function safeUp()
    {
        // Permissions

        // Permissions to manage users
        $registerUser = $this->auth->createPermission('registerUser');
        $registerUser->description = Yii::t('console', 'Permisson to create a new user');
        $this->auth->add($registerUser);
        
        $updateUser = $this->auth->createPermission('updateUserData');
        $updateUser->description = Yii::t('console','Permission to update user data and user profile');
        $this->auth->add($updateUser);
        
        $blockUser = $this->auth->createPermission('blockUser');
        $blockUser->description = Yii::t('console','Permission to blocking an user');
        $this->auth->add($blockUser);
        
        $deleteUser = $this->auth->createPermission('deleteUser');
        $deleteUser->description = Yii::t('console','Permission for the complete removal an user with all related data');
        $this->auth->add($deleteUser);
        
        // Group permission to manage access rules
        $manageAccess = $this->auth->createPermission('accessControl');
        $manageAccess->description = Yii::t('console','Permission to manage RBAC items');
        $this->auth->add($manageAccess);
        
        // Group permission to manage users
        $manageUsers = $this->auth->createPermission('manageUsers');
        $manageUsers->description = Yii::t('console','Permission to registration, updating users info and blocking user');
        $this->auth->add($manageUsers);
        $this->auth->addChild($manageUsers, $registerUser);
        $this->auth->addChild($manageUsers, $updateUser);
        $this->auth->addChild($manageUsers, $blockUser);
        
        // Roles
        /* Add rule to default roles */
        $defaultRoleRule = new DefaultRoleRule();
        $this->auth->add($defaultRoleRule);
        
        /* Base group roles*/
        $user = $this->auth->createRole('User');
        $user->description = Yii::t('console','Registered user');
        $user->ruleName = $defaultRoleRule->name;
        $this->auth->add($user);
        
        $admin = $this->auth->createRole('Administrator');
        $admin->description = Yii::t('console', 'The system administrator. Has access to content management and user management');
        $this->auth->add($admin);
        $this->auth->addChild($admin, $manageUsers);
        
        $root = $this->auth->createRole('Root');
        $root->description = Yii::t('console', 'A user with root access');
        $root->ruleName = $defaultRoleRule->name;
        $this->auth->add($root);
        $this->auth->addChild($root, $admin);
        $this->auth->addChild($root, $deleteUser);
        $this->auth->addChild($root, $manageAccess);
    }

    public function safeDown()
    {
        $this->auth->removeAll();
    }
    
    public function init() {
        $this->auth = Yii::$app->authManager;
        if(!$this->auth instanceof \yii\rbac\ManagerInterface)
        {
            throw new InvalidConfigException("To use RBAC, need to configure the component authManager.\n");
        }
        parent::init();
    }
}
