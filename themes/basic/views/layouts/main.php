<?php 

use yii\helpers\Html;
use app\themes\basic\ThemeAssets;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

/* @var $this \yii\web\View */
/* @var $content string */

ThemeAssets::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <div class="container">
        <?php $this->beginBody() ?>
        <?php  NavBar::begin([
            'brandLabel' => 'Ed-elaboration',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'nav',
            ],
        ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-left'],
                'items'=>[
                    ['label'=>'About','url'=>['/pages/frontend/index','page'=>'about']],
                    Yii::$app->user->isGuest?
                    ['label'=>'Login','url'=>['/user/security/login']] :
                    [
                        'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                        'url' => ['/user/security/logout'],
                        'linkOptions' => ['data-method' => 'post'],
                        'visible'=>!Yii::$app->user->isGuest
                    ],
                ]
            ]);
        NavBar::end()?>
        <?=$content?>
        
        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </div>
</body>
</html>
<?php $this->endPage() ?>

