<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\themes\basic;

/**
 * Description of ThemeAssets
 *
 * @author Tartharia
 */
class ThemeAssets extends \yii\web\AssetBundle{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@app/themes/basic/assets';
    
    public $css = [
        'css/site.css'
    ];


    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\web\YiiAsset'
    ];
}
