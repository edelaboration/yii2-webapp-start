<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\themes\adminLTE;

/**
 * Description of AdminLteJsAsset
 *
 * @author Tartharia
 */
class AdminLteJsAsset extends \yii\web\AssetBundle{
    public $sourcePath = "@bower/admin-lte/dist";
    
    public $js=[
        'js'=>'js/app.min.js'
    ];
}
