<?php

namespace app\common\models;

use Yii;
use dektrium\user\models\User as BaseUser;
use yii\rbac\ManagerInterface;

/**
 * Description of User
 *
 * @author Tartharia
 */
class User extends BaseUser{
    
    private $_access = [];
    
    /*public function getIsAdmin() {
        return Yii::$app->authManager instanceof ManagerInterface?Yii::$app->user->can('manageUsers'):parent::getIsAdmin();
    }*/
    
    public function can($itemName,$params=[])
    {
        if(!isset($this->_access[$itemName]))
        {
            $params['user'] = $this;
            $this->_access[$itemName] = Yii::$app->authManager->checkAccess($this->id, $itemName, $params);
        }
        return $this->_access[$itemName];
    }
    
    public function getRoles()
    {
        $authManager = Yii::$app->getAuthManager();
        $roles = $authManager->getRolesByUser($this->id);
        foreach ($authManager->defaultRoles as $role)
        {
            if($this->can($role))
                $roles[$role]=$authManager->getRole($role);
        }
        return $roles;
    }
}
