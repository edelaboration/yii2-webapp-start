<?php
return [
    'components' => [
        'db' => [
            'dsn' => 'mysql:host=localhost;dbname=basic_app_template',
            'username' => 'root',
            'password' => '',
        ],
        'mailer' => [
            'useFileTransport' => true,
            'fileTransportPath'=>'@app/mail/dummy'
        ]
    ],
];
