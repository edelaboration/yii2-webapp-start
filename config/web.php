<?php

$config = [
    'id' => 'webapp-start',
    'defaultRoute' => 'pages/frontend/index',
    'components' => [
        'assetManager'=>[
            'forceCopy'=>YII_DEBUG?true:false,
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => null,
                    'css' => []
                ]
            ]
        ],
        'authManager' => [
            'defaultRoles'=>['User','Root'],
            'cache' => 'cache'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'gE3WiADgDVouQdzQtbWHoQvcxRoAqzdU',
        ],
        'errorHandler' => [
            'errorAction' => 'pages/frontend/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                ''=>'/',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>' => '<_m>/<_c>/<_a>',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/view',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/<_a>'
            ],
        ],
        'view' => [
            'theme' =>[
                'class'=>'app\themes\basic\Theme',
            ]
        ],
        'user'=>[
            'identityClass'=>  'app\common\models\User',
            'loginUrl' => ['/user/security/login']
        ]
    ],
    'modules' => [
        'pages'=>[
            'class' => 'robote13\pages\Module'
        ],
        /*'publications' => [
            'class' => 'eapanel\publications\Module',
            'layout' => '@app/themes/adminLTE/views/layouts/main',
        ],*/
        'user' => [
            'layout' => '@app/themes/adminLTE/views/layouts/main',
            'adminPermission' => 'manageUsers',
            'controllerMap'=>[
                'security'=>[
                    'class' => \dektrium\user\controllers\SecurityController::className(),
                    'layout' => '/main'
                ],
                'registration'=>[
                    'class' => \dektrium\user\controllers\RegistrationController::className(),
                    'layout' => '/main'
                ],
                'recovery'=>[
                    'class' => dektrium\user\controllers\RecoveryController::className(),
                    'layout' => '/main'
                ]
            ],
            'modelMap'=>[
                'User' => '\app\common\models\User'
            ]
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\Module',
            'layout' => '@app/themes/adminLTE/views/layouts/main',
            'as access' => [
                'class' =>  yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['accessControl']
                    ]
                ]
            ]          
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'robote13\gii\Module'
    ];
}

return $config;
