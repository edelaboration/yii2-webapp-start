<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

return [
    'id' => 'webapp-start-console',
    'language' => 'en',
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'app\commands',
    'components'=>[
        'mailer' => [
            'useFileTransport'=>YII_DEBUG,
            'fileTransportPath'=>'@app/mail/dummy'
        ]
    ],
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
];
