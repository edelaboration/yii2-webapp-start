<?php
use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(
    require(__DIR__ . DIRECTORY_SEPARATOR . 'params.php'),
    require (__DIR__ .DIRECTORY_SEPARATOR .'params-loc.php')
);

if(YII_DEBUG)
{
    $targets = [
        [
            'class'=>'yii\log\SyslogTarget',
            'levels'=>['trace']
        ]
    ];
} else {
    $targets = [
        [
            'class'=> 'yii\log\EmailTarget',
            'levels'=>['error','warning'],
            'mailer'=>'mailer',
            'message'=>[
                'from'=>'noreply@site.ru',
                'to'=>$params['adminEmail'],
                'subject'=>'Site.ru log'
            ]
        ]
    ];
}

return [
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru',
    'components' => [
        'authManager' => [
            'class' => dektrium\rbac\components\DbManager::className(),
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport'=>YII_DEBUG,
            'fileTransportPath'=>'@app/mail/dummy'
        ],
        'log' => [
            'class'=>'yii\log\Dispatcher',
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'=>$targets
        ],
        'db'=> [
            'class'=>'\yii\db\Connection',
            'dsn'=>'mysql:host=localhost;dbname=db_name',
            'username'=>'username',
            'password'=>'password',
            'charset'=>'utf8'
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => yii\i18n\PhpMessageSource::className(),
                    'fileMap' => [
                        'app' => 'app.php',
                    ],
                ]
            ],
        ],
    ],
    'modules' => [
        'rbac' => [
            'class' => 'dektrium\rbac\Module',
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'admins' => []
        ]
    ],
    'params' => $params,
];