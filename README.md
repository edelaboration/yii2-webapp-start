Yii 2 Web Application Start Template
================================

Yii 2 Web Application Start Template is a skeleton Yii 2 application best for
rapidly creating small projects.

DIRECTORY STRUCTURE
-------------------

      commands/           contains console commands (controllers)
      config/             contains application configurations
      mail/               contains view files for e-mails
      messages/           contains common translation files
      migrations/         contains migrations classes
      modules/
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      themes/             содержит темы оформления
      vendor/             contains dependent 3rd-party packages
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this application template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this application template using the following command:

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar create-project --prefer-dist --stability=dev eapanel/yii2-webapp-start new_app
~~~

Now you should be able to access the application through the following URL, assuming `new_app` is the directory
directly under the Web root.

~~~
http://localhost/new_app/web/
~~~


CONFIGURATION
-------------

### Database

Edit the file `config/common.php` with real data.

**NOTE:** Yii won't create the database for you, this has to be done manually before you can access it.

Also check and edit the other files in the `config/` directory to customize your application.

### Migrations

~~~
yii migrate --migrationPath=@yii/rbac/migrations
yii migrate --migrationPath=@dektrium/user/migrations
yii migrate
~~~